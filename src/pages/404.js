import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import FlatButton from './components/FlatButton';
import MainPanel from './components/MainPanel';


class NotFound extends Component {

  render() {
    return (
      <MainPanel>
        <div className="mx-auto col-12">
          <div className="aling-center mt-4 mb-4">
            <img  className="logo wow fadeInUp" src="img/logo.png" alt="logo" />
          </div>  
          <h1 className="title wow fadeInUp text-center">Pontuou</h1>
          <br/> 
          <h2 className="wow fadeInUp some-text text-center" data-wow-delay="0.1s">
            Essa página não existe. <span role="img" aria-label="emoji chateado">😕</span> 
          </h2>
          <br />
          <Link to="/"><FlatButton text="Página Inicial"/></Link>
        </div>
      </MainPanel>
    )
  }
}

export default NotFound;
