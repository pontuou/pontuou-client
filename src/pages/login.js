import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import WOW from 'wowjs';
import IMask from 'imask';
import RequestHelper from '../utils/request-helper';
import { urlCards, urlLogin } from '../utils/urls';
import { setToken } from '../utils/local-storage';
import Message from './components/Message';
import FlatButton from './components/FlatButton';
import MainPanel from './components/MainPanel';
import {setFocus} from '../utils/helpers';
// import FooterNav from './components/FooterNav';

class Login extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      logged_in: undefined,
      error: undefined
    }
    this.inputValue = null;
  }

  componentDidMount(){
    this.WOW.init();
    this.getToken();

    const element = document.getElementById("identifier");
    const mask = { 
        mask: '(00) 0 0000 0000',
      }
    this.inputValue = new IMask(element, mask);
  }

  getToken = async () => {
    const username = "enterprise@gmail.com";
	  const password = "123";

    await RequestHelper('post', urlLogin, {username, password}, 'load')
    .then(res => {
      setToken("Bearer " + res.token);
    })
    .catch(err => {
      console.log(err);
      if(err){
        this.setState(() => {
          return {error: err.data.message}
        })
      }
    })
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const finalClientUI = this.inputValue.unmaskedValue;
    await RequestHelper('post', urlCards, {finalClientUI}, 'load', true)
    .then(res => {
      this.setState(() => ({ logged_in: true, data: res }))
    })
    .catch(err => {
      let message = '';
      console.log(err)
      if(err.status === 401) message = 'Sua sessao expirou. Atualize sua pagina. =)';
      this.setState(() => ({ error: message || err.data.message }));
    })

  }

  render() {
    if (this.state.logged_in === true) {
      return <Redirect push to={{pathname: "/cards", data: this.state.data}} />
    }
    return (
      <MainPanel>
        <div className="mx-auto col-12">
          <div className="aling-center mt-4 mb-4">
            <img  className="logo wow fadeInUp" src="img/logo.png" alt="logo" />
          </div>  
          <h1 className="title wow fadeInUp text-center">Pontuou</h1>
          <br/> 
          <h2 className="wow fadeInUp some-text text-center" data-wow-delay="0.1s">
            Veja aqui todos os seus cartões fidelidade!
          </h2>
          <br/>
          <form onSubmit={this.handleSubmit}>
            <div className="card mx-xl-5">
              <div className="card-body load">
                {this.state.error && <Message type="danger" text={this.state.error} />}
                <label htmlFor="identifier" className="grey-text font-weight-light">Telefone</label>
                <input 
                  required 
                  type="tel" 
                  id="identifier" 
                  name="identifier" 
                  className="form-control number" 
                  ref={field => field && setFocus(field)} 
                  placeholder="Ex.: 88 9 1234 5678"/>
                <div className="text-center py-4 mt-3">
                  <FlatButton type="submit" text="Pesquisar" icon="fa fa-search ml-2" id="btn-login"/>
                </div>
              </div>
            </div>
          </form>
        </div>
      </MainPanel>
    )
  }
}

export default Login;
