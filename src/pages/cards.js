import React, {Component} from 'react';
import {Link, Redirect} from 'react-router-dom'
import WOW from 'wowjs';

import FlatButton from './components/FlatButton';
import StoreCard from './components/StoreCard';
import Message from "./components/Message";
import MainPanel from './components/MainPanel';


const CardsItem = (props) => {
  const { cards } = props;
  return (cards.map(card =>
    <StoreCard
      key={card.cardId}
      enterpriseLogoUrl={card.enterprise.enterpriseLogoUrl}
      place={card.enterprise.enterpriseName}
      offers={card.offers}
    />
  ));
}

class Cards extends Component {
  constructor() {
    super();
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      message: undefined,
      client: undefined,
      cards: undefined,
      redirect: false,
    }
  }

  componentDidMount(){
    this.WOW.init({live: false});
    if(this.props.location.data){
      const { cards, ...client } = this.props.location.data;
      this.setState({
        client: client,
        cards: this.mergeCards(cards)
      });
    } else {
      this.setState({redirect: true});
    }
  }

  mountEnterprise = (card) => {
    let { offer, ...enterprise } = card;
    enterprise.offers = [];
    enterprise.offers.push(offer);
    return enterprise;
  }
  
  mergeCards = (cards) => {
    const myMap = new Map();
    // eslint-disable-next-line
    cards.map(card => {
      const { currentStampQuantity, maxStampQuantity} = card;
      card.offer.currentStampQuantity = currentStampQuantity;
      card.offer.maxStampQuantity = maxStampQuantity;

      if(myMap.has(card.enterprise.enterpriseId)){
        myMap.get(card.enterprise.enterpriseId).offers.push(card.offer);
      } else {
        myMap.set(card.enterprise.enterpriseId, this.mountEnterprise(card));
      }      
    });
    return Array.from(myMap, ([key, value]) => value);
  }

  render() {
    const { cards, client, redirect } = this.state;

    if (redirect) {
      return <Redirect to="/" />
    }

    return (
      client ?
        <MainPanel >
          <div className="show-cards">

            <div className="mt-3 mb-3 aling-center">
              <img  className="logo fadeInUp" src="img/logo.png" alt="logo" />
            </div>

            <h1 className="title fadeInUp text-center mb-2">
              Pontuou
            </h1>
            <h2 className="fadeInUp some-text text-center" data-wow-delay="0.1s" >
              {`Olá`} <span className="name-client"> {client.finalClientEmail}</span>
              <span role="img" aria-label="coração"> 💛</span> {`, aqui estão seus cartões.`}
            </h2>
            <br/>
            { this.state.message && <Message type="success" text={this.state.message} /> }
            <CardsItem  cards={cards}/>
          </div>

          <div className="text-center">
            <Link to="/">
            <FlatButton icon="fa fa-arrow-left  ml-2" text="Voltar" type="button"/>
            </Link>
          </div>
        </MainPanel>
        :
        <div></div>
    )
  }
}

export default Cards;
