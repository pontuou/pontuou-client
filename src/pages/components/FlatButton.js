import React, {Component} from 'react';

export default class FlatButton extends Component {

  render() {
    var { color } = this.props;
    if(!color) color = 'button-accent';
    return (
      <div className="text-center">
        <button className={`btn ${color} btn-rounded`} 
          onClick={this.props.click} 
          type={this.props.type} 
          id={this.props.id}
        > 
          {this.props.text}
          <i className={this.props.icon}></i>
        </button>
      </div>
    )
  }
}
