import React, {Component}from 'react';

class FooterNav extends Component {

  render() {
    return (
      <nav className="navbar fixed-bottom">
        <div className="alert alert-light alert-dismissible fixed-bottom rounded-0 text-center" role="alert">
          <p className="pt-3 pr-2 p-info">Use o telefone <strong className="number" >88 9 8888 8888</strong> para ver o funcionamento!</p>
          <button type="button" className="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </nav>
    );
  }
}

export default FooterNav;