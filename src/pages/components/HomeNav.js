import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import Link from 'react-router-dom/Link';

class HomeNav extends Component {

  render() {
    return (
      <header>
        <nav className="navbar fixed-top navbar-dark navbar-expand-lg bg-darkPurple scrolling-navbar">
            <ul className="navbar-nav ml-auto ">
              <li className="nav-item">
                <Link to={{pathname: "/sobre"}} style={{ textDecoration: 'none', color: 'white', height: '5em' }} >
                Sobre
                <FontAwesomeIcon icon={faInfoCircle} size="lg" id="info-icon" color="white" />                
                </Link>
              </li>
            </ul>
        </nav>
      </header>
    )
  }
} 

export default HomeNav;