import React, { Fragment } from 'react';
import Check from './Check';

const checkType = (offer) => {
  if(offer.offerType === "POINTS") {
    return calculateStars(offer.currentStampQuantity, offer.maxStampQuantity);
  }else{
    return renderBar(offer.currentStampQuantity, offer.maxStampQuantity);
  }
}

const renderBar = (real, maximum) => (
  <div className="align-center col-12 m-0">
    <h5>R$ {real} / {maximum}</h5>
    <progress max={maximum} value={real} className="w-100"></progress>
  </div>
)

const calculateStars = (real, maximum) => {
  const checks = [];
  let typeStar;
  let count = real;
  const max = maximum;
  for(let i = 0; i < max; i++) {
    if(i < count) typeStar = 'star';
    if(i >= count) typeStar = 'no-star';
    checks.push(
      <Check
        type={typeStar}
        key={i}
      />
    );
  }
  return checks;
}

const OfferItems = (props) => {
  const { offers} = props;

  const offerItems = offers.map(offer => 
    <div key={offer.offerId}>
      <hr/>
      <h4 className="mb-2" >{offer.offerDescription || ''} </h4>
      <div className="mb-2 p-0 div-stars aling-center">
        {checkType(offer)}
      </div>
    </div>
  )
  return offerItems;
} 


export default (props) => {
  return (
    <Fragment>
      <div className="my-cards card mx-xl-5">
        <div className="card-body text-center load pb-0">
          <div className="media d-block d-md-flex mt-md-0">
            <div className="col-md-4 d-flex align-self-center mr-3 mt-0">
              <img className="d-flex mb-md-0 mb-3 avatar-2 rounded-circle z-depth-1 mx-auto" 
                src={props.enterpriseLogoUrl ? props.enterpriseLogoUrl : 'img/icon.png'} alt={props.place} width="100" height="100" />
            </div>
            <div className="media-body mt-0">
              <div className="col-md-10">
                <div className="">
                  <h4>{props.place || ''}</h4>
                  <OfferItems offers={props.offers} />
                  <br/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br/>
    </Fragment>
  )
}
