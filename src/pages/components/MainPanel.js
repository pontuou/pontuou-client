import React from 'react';
import HomeNav from './HomeNav';

export default (props) => {
  return (
    <main>
      <div className="jarallax" data-jarallax='{"speed": 0.1}'>
        <HomeNav />
        <div className="container">
          <div className="row wow fadeIn horizontalCenter">
            <div className="col-md-12 ">
              <div className="wow fadeInUp" data-wow-delay="0.4s">
                {props.children}
              </div>
              <br/>
            </div>
          </div>
        </div>
      </div>
    </main>
  )
}
