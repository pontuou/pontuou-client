import React, { Component } from 'react';

class NavBar extends Component {

  handleClick = (event, id) => {
    event.preventDefault();
    document.getElementById(id).scrollIntoView();
  }

  render() {
    return (
      <nav className="navbar fixed-top navbar-dark navbar-expand-lg bg-darkPurple scrolling-navbar">
        <div className="container">

          <a className="navbar-brand" href="/">
            <img  className="d-inline-block align-top mr-2" src="img/logo.png" alt="logo" width="30" height="30"/>
          </a>

          <button className="navbar-toggler" type="button" data-toggle="collapse"  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" data-target="#navbarSupportedContent">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">

            <ul className="navbar-nav mr-auto">
            </ul>
            <ul className="navbar-nav nav-flex-icons">
              <li className="nav-item">
                <a className="nav-link" href="#about" onClick={e => this.handleClick(e, "about")}>Vantagens</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#demo" onClick={e => this.handleClick(e, "demo")}>Demostração</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#contact" onClick={e => this.handleClick(e, "contact")}>Contato</a>
              </li>
              <li className="nav-item d-none">
                <a href="https://www.facebook.com/pontuou" className="nav-link" target="_blank" rel="noopener noreferrer">
                  <i className="fa fa-facebook"></i>
                </a>
              </li>
              <li className="nav-item d-none">
                <a href="https://twitter.com/pontuou" className="nav-link" target="_blank" rel="noopener noreferrer">
                  <i className="fa fa-twitter"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}

export default NavBar;