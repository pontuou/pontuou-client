import React from 'react';

export default () => (
  <div className="container mt-5" id="contact">
    <hr className="hr hr-light my-5"/>
    <h3 className="white-text text-center mb-5 h3">Contatos</h3>

    <div className="row">
      <div className="col-lg-6 col-md-12 text-center mb-4 border-right white-text">
        <i className="far fa-envelope fa-3x mb-3"></i>
        <a href="mailto:pontuou@gmail.com" className="d-block white-text btn-lg button-orange h4"> pontuou@gmail.com </a>
      </div>

      <div className="col-lg-6 col-md-12 text-center mb-4 white-text">
        <i className="fab fa-whatsapp fa-3x mb-3"></i>
        <a href="https://web.whatsapp.com/send?phone=558896283716text=Pontuou" className="d-block white-text btn-lg button-orange h4">Fale conosco pelo WhatsApp</a>
      </div>
    </div>

  </div>
)