import React, { Component } from 'react';
import imgStar from '../../images/star.png';

class About extends Component {
  render() {
    return(
      <div className="container" id="about">
        <hr className="hr-light my-5"/>

        <section  className="mr-5 ml-5">

          <h3 className="h3 white-text text-center mb-5">Vantagens</h3>

          <div className="row wow fadeIn">

            <div className="col-md-12 px-4">

              <div className="row mb-4">
                <div className="col-1 p-0">
                  <img src={imgStar} className="img-fluid lp-star" alt="start"></img>
                </div>
                <div className="col-10">
                  <h5 className="white-text feature-title bold">Economia</h5>
                  <p className="white-text">Diga adeus a impressão de cartães. Poupe dinheiro e ajude o meio ambiente.</p>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col-1 p-0">
                  <img src={imgStar} className="img-fluid lp-star" alt="start"></img>
                </div>
                <div className="col-10">
                  <h5 className="white-text feature-title bold">Agilidade</h5>
                  <p className="white-text">Facilidade e velocidade para pontuar seus clientes. </p>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col-1 p-0">
                  <img src={imgStar} className="img-fluid lp-star" alt="start"></img>
                </div>
                <div className="col-10">
                  <h5 className="white-text feature-title bold">Informação</h5>
                  <p className="white-text">Saiba exatamente o quanto foi vendido, quantos cliente são novos e quantos estão de volta no seu estabelecimento.</p>
                </div>
              </div>
            </div>
            
            <div className="col-md-12" id="demo">
            <hr className="hr-light my-5"></hr>
              <h3 className="h3 white-text text-center mb-5">Conheca o Pontuou!</h3>
              <a className="btn btn-rounded button-orange btn-lp" target="_black" href="docs/pontuou.pdf">Veja uma demostração!</a>
            </div>
          </div>

        </section>
      </div>
    )
  }
}

export default About;