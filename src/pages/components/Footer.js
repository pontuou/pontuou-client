import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return(
      <footer className="page-footer text-center font-small mt-4 wow fadeIn">
        <div className="pb-4 d-none">
          <a href="https://www.facebook.com/pontuou" target="_blank" rel="noopener noreferrer">
            <i className="fa fa-facebook mr-3"></i>
          </a>
          <a href="https://twitter.com/pontuou" target="_blank" rel="noopener noreferrer">
            <i className="fa fa-twitter mr-3"></i>
          </a>
        </div>
        <div className="footer-copyright py-3">
          © <span className="number" >2019</span> - Todos direitos reservados. <span className="number" >2019</span>.
          <a href="https://pontuou.com/"> pontuou.com </a>
        </div>
      </footer>
    )
  }
}

export default Footer;