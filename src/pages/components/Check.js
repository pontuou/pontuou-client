import React from 'react';

import no_star from '../../images/no_star.png';
import star from '../../images/star.png';

export default function Check (props) {
  let starImg;
  switch (props.type) {
    case 'no-star':
      starImg = no_star;
      break;

    default:
      starImg = star;
      break;
  }
  return <img src={starImg} className="stars" alt="Carimbo"/>;
}