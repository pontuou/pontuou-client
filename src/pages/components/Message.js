import React from 'react';

export default function Message(props) {
  let type = '';
  switch(props.type) {
    case 'info':
      type = "message btn-info";
      break;
    case 'warning':
      type = "message btn-warning";
      break;
    case 'success':
      type = "message btn-success";
      break;
    case 'danger':
      type = "message btn-danger";
      break;
    default:
      type = "none";
  }
  return <p className={type}>{props.text}</p>;
}
