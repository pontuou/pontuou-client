import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import NavBar from './components/NavBar';
import WOW from 'wowjs';
import About from './components/About';
import Footer from './components/Footer';
import Contact from './components/Contact';


class LandingPage extends Component {

  constructor(props) {
    super(props);
    this.WOW = new WOW.WOW({live: false});
    this.state = {
      logged_in: undefined,
      error: undefined
    }
    this.inputValue = null;
  }

  render() {
    return(
      <div className="view full-page-intro">
        <NavBar/>
        <div className="d-flex justify-content-center align-items-center pt-80">
          <div className="container">
            <div className="row wow">
              <div className="col-md-8 mb-4 white-text text-center text-md-left">

                <div className="media d-flex align-items-center">
                  <div className="col-lg-3">
                    <img className="d-flex align-self-center mr-3 img-fluid" src="img/logo.png" alt="logo" />
                  </div>
                  <h1 className="col-lg-9 h1">Pontuou</h1>
                </div>

                <hr className="hr-light"/>
                <h2>
                  <strong>A melhor forma de fidelizar os seus clientes.</strong>
                </h2>
                <p className="mb-4 d-none d-md-block">
                  <strong>
                    Através do Pontuou, você fica sabendo quais os seus clientes mais fiéis. 
                    Podendo lhes oferecer prêmios de acordo com a frequência que visitam seu estabelecimento.
                  </strong>
                </p>
              </div>
              <div className="col-md-4 col-xl-4 mb-4">
                <br/>
                <br/>
                <div className="card">
                  <div className="card-body">
                      <h3 className="dark-grey-text text-center">
                        <strong>Já pontuou?</strong>
                        <br/>
                        <Link to="/" className="btn btn-rounded button-orange btn-lp">Veja seus pontos!</Link>
                      </h3>
                      <hr/>
                      <h3 className="dark-grey-text text-center">
                        <strong>Quer pontuar seus clientes?</strong>
                        <br/>
                        <a href="https://empresa.pontuou.com" className="btn btn-rounded button-orange btn-lp">Pontue agora!</a>
                      </h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <About />
        <Contact />
        <Footer />
      </div>
    )
  };
}

export default LandingPage;