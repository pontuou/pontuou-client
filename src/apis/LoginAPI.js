import {urlLogin} from '../utils/urls';
import RequestHelper from '../utils/request-helper';
import Actions from '../utils/actions';
import {setToken, clearKey, setEnterpriseEmail} from '../utils/local-storage';

export default class LoginAPI {
  static requestLogin(username, password) {
    return dispatch => {
      RequestHelper('post', urlLogin, {username, password})
      .then(result => {
        console.log('API', result);
        dispatch(Actions('LOGIN', result));
        setToken(result);
        setEnterpriseEmail(username);
      })
      .catch(err => {
        clearKey('fdc-token')
        console.log('API', err);        
      })
    }
  }

}