import Actions from '../utils/actions';
import RequestHelper from '../utils/request-helper';
import {urlStamp} from '../utils/urls';
import {getData} from '../utils/local-storage';

export default class PurchaseAPI {
  // static checkClient (finalClientUI, enterpriseOwnerEmail) {
  //   return dispatch => {
  //     RequestHelper('post', urlExistsClient, {finalClientUI, enterpriseOwnerEmail: getData().enterpriseEmail}, true)
  //     .then(result => {
  //       console.log(result);
  //       dispatch(Actions('CLIENT-EXISTS', result));
  //     })
  //     .catch(err => {
  //       if (err.status === 400) {
  //         dispatch(Actions('CLIENT-DONT-EXISTS', finalClientUI))
  //       }
  //       console.log(err);
  //     })
  //   }
  // }

  // static register (uniqueIdentifier, email) {
  //   return dispatch => {
  //     RequestHelper('post', urlCreateUser, {uniqueIdentifier, email, enterpriseOwnerEmail: getData().enterpriseEmail}, true)
  //     .then(result => {
  //       console.log(result);
  //       dispatch(Actions('REGISTER-SUCCESS', result));
  //     })
  //     .catch(err => {
  //       console.log(err);
  //     })
  //   }
  // }

  static stamp(finalClientId) {
    return dispatch => {
      RequestHelper('post', urlStamp, {finalClientId, enterpriseOwnerEmail: getData().enterpriseEmail}, 'load', true)
      .then(result => {
        console.log(result);
        dispatch(Actions('STAMP-SUCCESS', result))
      })
      .catch(err => {
        console.log(err);
      })
    }
  }
}