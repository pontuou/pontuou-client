import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Login from './pages/login';
import Cards from './pages/cards';
import LandingPage from './pages/landingpage';
import NotFound from './pages/404';

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Login}/>
            <Route exact path="/cards" component={Cards}/>
            <Route exact path="/sobre" component={LandingPage}/>
            <Route path="/*" component={NotFound}/>
        </Switch>
    </BrowserRouter>
);

export default Routes;