//  DEVELOPMENT
//const base = 'https://pontuou-srv-desenvolvimento.herokuapp.com/v1.4/';

//  PRODUCTION
const base = 'https://pontuou-srv-production.herokuapp.com/v1.5/';

export const urlLogin = base + 'login';

export const urlCards = base + 'finalClients/getAllCardsByUI';