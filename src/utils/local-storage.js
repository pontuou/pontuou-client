const tokenTag = 'fdc-token';


export function getData() {
  return {
    token: localStorage.getItem(tokenTag)
  }
}

export function setToken(token) {
  localStorage.setItem(tokenTag, token)
}

export function clearKey(key){
  localStorage.removeItem(key)
}