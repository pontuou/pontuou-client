export default function(type, content) {
  return {
    type,
    content
  }
}